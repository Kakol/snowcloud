#!/bin/bash
installAs='Air for Steam'
installPath="$HOME/.local/share/Steam/skins"
rm -rf 'build' "$installAs"
mkdir 'build'
cd 'build'
git init
git pull 'https://github.com/airforsteam/Air-for-Steam'
rm -rf '.git'
rm 'Changelog.url' 'FAQ.url' 'README.md' 'Support Air on Patreon.url' 'Contributors.url' # Clean up un-needed files
mv 'LICENSE.txt' 'licence'
cp './../config.ini' './'
cd './../'

mkdir -p "$installPath"
mv './build' "$installAs"
cp -r "$installAs" "$installPath"

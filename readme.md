# Snowcloud
A simple script written in Bash to download the latest version of Air for Steam and push it, along with your config to Steam.

## Usage
```bash
# chmod +x ./snowcloud.sh
./snowcloud.sh
```
```bash
curl -sSL https://gitlab.com/Kakol/snowcloud/raw/master/snowcloud.sh | bash
```